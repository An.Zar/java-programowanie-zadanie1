package pl.sda;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        LocalDate today = LocalDate.now();
        System.out.println(today);

        System.out.println("Proszę podać datę zajęć w formie YYYY-MM-DD");
        Scanner sc = new Scanner(System.in);
        String meetDate = sc.nextLine();
        LocalDate meetDateLD = LocalDate.parse(meetDate);
        System.out.println(meetDateLD);

        Period period = Period.between(today, meetDateLD);
        System.out.println("Do rozpoczęcia zajęć pozostało " + period.getDays() + " dni");

    }
}
